def generate_words(word1: str) -> list[str]:
	return {w.strip() for w in open('sowpods.txt') if len(w) == len(word1) + 1}

def convert(word1: str, word2: str) -> list[str]:
	words = generate_words(word1) - {word1}
	track = [word1]

	while word1 != word2:
		for w1, w2 in zip(word1, word2):
			if word1.replace(w1, w2) in words:
				word1 = word1.replace(w1, w2)  
				track.append(word1)
				words = words - {word1}
			
	return ' => '.join(track)

print(convert("CLAY", "GOLD"))
print(convert("CATS", "MICE"))
print(convert("PONY", "COLT"))

